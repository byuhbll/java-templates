# Java Executable Archetype #

Java Executable Archetype is a maven archetype designed to create a simple ready to use java standalone application project 
geared towards our development environment. It comes with a ready made pom familiar with our build processes and a main
method to get you going quickly. Execute the following command from the command line to invoke the archetype.

    mvn archetype:generate -DarchetypeGroupId=edu.byu.hbll.maven -DarchetypeArtifactId=executable-archetype -DarchetypeVersion=2.0.0

You'll then be prompted to enter the following

* artifactId: the artifactId which is generally the name of your project.
* package: the base package name of your project. This is generally the same as groupId.artifactId (ie, edu.byu.hbll.project) except when the artifactId contains invalid characters or if you have another reason for it not to be the same.
* repository: name of the git repository
* description: brief project description

To build the resulting application, run the following command:

    mvn clean compile assembly:single

This will create a file with the name `artifact-version-jar-with-dependencies.jar` in the target directory.
