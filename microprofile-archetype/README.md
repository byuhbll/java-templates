# Microprofile Archetype #

Microprofile Archetype is a maven archetype designed to create a simple ready to use Eclipse MicroProfile project geared towards our 
development environment. It comes with a ready made pom familiar with our build processes and a Ping class to verify 
deployment. Execute the following command from the command line to invoke the archetype.

    mvn archetype:generate -DarchetypeGroupId=edu.byu.hbll.maven -DarchetypeArtifactId=microprofile-archetype -DarchetypeVersion=2.0.0

You'll then be prompted to enter the following

* artifactId: the artifactId which is generally the name of your project.
* package: the base package name of your project. This is generally the same as groupId.artifactId (ie, 
edu.byu.hbll.project) except when the artifactId contains invalid characters or if you have another reason for it not 
to be the same.
* contextRoot: the context root of your application. This is generally the same as the artifactId.
* repository: name of the git repository
* description: brief project description

To deploy your newly created project to GlassFish, run the following maven commands.

    mvn package glassfish:deploy