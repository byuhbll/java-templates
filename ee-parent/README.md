# Java EE Parent POM #

This is the parent pom for all BYU HBLL Java EE Projects. The primary components that this POM provides are the Java EE
api dependency, the edu.byu.hbll:config library, the edu.byu.hbll:swagger library, and dependencies necessary for 
logback.

This POM can be referenced by adding the following to an existing POM:

```xml
<parent>
  <groupId>edu.byu.hbll.maven</groupId>
  <artifactId>ee-parent</artifactId>
  <version>2.0.0</version>
</parent>
```