# Maven Infrastructure #
This repository contains a multi-module maven project that provides infrastructure components for the 
BYU Library's maven projects. 

**Deprecation Notice**: This project is no longer supported.  Use https://gitlab.com/byuhbll/lib/java/maven-objects instead.

## Reference ##
The following is a quick-reference guide on how to use each component. More information is available
at the links.

#### [ Library Parent POM ](library-parent/) ####
Add the following to an existing POM:

```xml
<parent>
  <groupId>edu.byu.hbll.maven</groupId>
  <artifactId>library-parent</artifactId>
  <version>2.0.0</version>
</parent>
```

#### [ EE Parent POM ](ee-parent/) ####
Add the following to an existing POM:

```xml
<parent>
  <groupId>edu.byu.hbll.maven</groupId>
  <artifactId>ee-parent</artifactId>
  <version>2.0.0</version>
</parent>
```

#### [ Executable Parent POM ](executable-parent/) ####
Add the following to an existing POM:

```xml
<parent>
  <groupId>edu.byu.hbll.maven</groupId>
  <artifactId>executable-parent</artifactId>
  <version>2.0.0</version>
</parent>
<properties>
  <application.entrypoint>edu.byu.hbll.app.Main</application.entrypoint>
</properties>
```

#### [ MicroProfile Parent POM ](microprofile-parent/) ####
Add the following to an existing POM:

```xml
<parent>
  <groupId>edu.byu.hbll.maven</groupId>
  <artifactId>microprofile-parent</artifactId>
  <version>2.0.0</version>
</parent>
```

#### [ Library Archetype ](library-archetype/) ####
Execute the following from the command line to invoke the archetype:

    mvn archetype:generate -DarchetypeGroupId=edu.byu.hbll.maven -DarchetypeArtifactId=library-archetype -DarchetypeVersion=2.0.0

#### [ EE Archetype ](ee-archetype/) ####

    mvn archetype:generate -DarchetypeGroupId=edu.byu.hbll.maven -DarchetypeArtifactId=ee-archetype -DarchetypeVersion=2.0.0

#### [ Executable Archetype ](executable-archetype/) ####

    mvn archetype:generate -DarchetypeGroupId=edu.byu.hbll.maven -DarchetypeArtifactId=executable-archetype -DarchetypeVersion=2.0.0

#### [ Angular Archetype ](angular-archetype/) ####

    mvn archetype:generate -DarchetypeGroupId=edu.byu.hbll.maven -DarchetypeArtifactId=angular-archetype -DarchetypeVersion=2.0.0

#### [ MicroProfile Archetype ](microprofile-archetype/) ####

    mvn archetype:generate -DarchetypeGroupId=edu.byu.hbll.maven -DarchetypeArtifactId=microprofile-archetype -DarchetypeVersion=2.0.0
