# Microprofile Parent POM #

This is the parent pom for all BYU HBLL Eclipse MicroProfile Projects. The primary components that this POM provides are the MicroProfile
dependency, jax-rs api 2.1, the edu.byu.hbll:config library, and dependencies necessary for logback.

This POM can be referenced by adding the following to an existing POM:

```xml
<parent>
  <groupId>edu.byu.hbll.maven</groupId>
  <artifactId>microprofile-parent</artifactId>
  <version>2.0.0</version>
</parent>
```