println "finalizing .gitignore"
def file = new File("${artifactId}/.gitignore")
def fileReplaced = file.text.replaceAll("__rootArtifactId__", "${artifactId}")
file.text = fileReplaced

println "Attempting to generate new angular project in ${artifactId}/${artifactId}-frontend."
println "Using the globally installed version of the angular cli (ng)."
def version = "ng --version".execute()
println version.text

def installedVersion = "npm list --depth=0 -g | grep @angular/cli".execute()
def installedVersionText = installedVersion.text
def parsedInstalledVersion = installedVersionText.substring(installedVersionText.lastIndexOf('@') + 1).trim()
println "Installed Angular cli version - " + parsedInstalledVersion

def availableVersion = "npm show @angular/cli version".execute()
def availableVersionText = availableVersion.text
println "Available Angular cli version - " + availableVersionText

if( parsedInstalledVersion != availableVersionText.trim() ){
    println "The installed version of the Angular CLI is not the latest version. We'll try generating a new angular project, but if it doesn't work, update the Angular CLI to the latest version."
}

println "Generating angular project. This may take a few minutes."

def proc = "ng new ${artifactId}-frontend --directory ${artifactId}/${artifactId}-frontend".execute()
def procerr = new StringBuffer()
proc.consumeProcessErrorStream(procerr)

println proc.text
println procerr.toString()

"rm -rf ${artifactId}/${artifactId}-frontend/.git".execute();
"echo target/ >> ${artifactId}/${artifactId}-frontend/.gitignore".execute()
