#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.api;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/** Application class. */
@ApplicationPath("api")
public class MyApplication extends Application {}
