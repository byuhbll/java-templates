# Java Library Archetype #

Java Library Archetype is a maven archetype designed to create a simple ready to use java project geared towards creating a java
library. It comes with a ready made pom familiar with our build processes and a main method for testing. Execute 
the following command from the command line to invoke the archetype.

    mvn archetype:generate -DarchetypeGroupId=edu.byu.hbll -DarchetypeArtifactId=java-project -DarchetypeVersion=2.0.0

You'll then be prompted to enter the following

* artifactId: the artifactId which is generally the name of your project.
* package: the base package name of your project. This is generally the same as groupId.artifactId (ie, edu.byu.hbll.project) except when the artifactId contains invalid characters or if you have another reason for it not to be the same.
* repository: name of the git repository
* description: brief project description

To execute the HelloWorld main method, run the following command, but change the package to the correct one.

    mvn exec:java -Dexec.mainClass="edu.byu.hbll.project.HelloWorld"
