#!/bin/bash

echo "#!/bin/bash" > test.sh
echo "set -e" >> test.sh

cat bitbucket-pipelines.yml | grep "\ \-\ " | grep -v "\-\ step\:" | sed --expression='s/          - //g' >> test.sh
chmod a+x test.sh
./test.sh
rm test.sh
rm -rf /var/tmp/*