# Java Executable Parent POM #

This is the parent pom for all BYU HBLL standalone java applications. The main components that this POM provides are the
dependencies necessary for logback, the edu.byu.hbll:config library, and a build process that produces a single jar
that contains all the application dependencies.

This POM can be referenced by adding the following to an existing POM, replacing `${mainclass}` with the fully
qualified classname that contains the entrypoint (main method) for the application:

```xml
<parent>
  <groupId>edu.byu.hbll.maven</groupId>
  <artifactId>executable-parent</artifactId>
  <version>2.0.0</version>
</parent>
<properties>
  <application.entrypoint>${mainclass}</application.entrypoint>
</properties>
```

A child project can be built by running the following command:

    mvn clean package

This will create a file with the name `{artifactId}.jar` in the target directory.